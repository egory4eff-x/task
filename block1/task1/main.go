package main

import "fmt"

type Student struct {
	Name   string
	Age    int
	Module []Course
}

type Course struct {
	Class, Rating string
}

func main() {
	st := []Student{
		{
			Name: "Tom",
			Age:  12,
			Module: []Course{
				{
					Class:  "eng",
					Rating: "5",
				},
				{
					Class:  "math",
					Rating: "3",
				},
			},
		},
		{
			Name: "bob",
			Age:  11,
			Module: []Course{
				{
					Class:  "fiz",
					Rating: "5",
				},
				{
					Class:  "bot",
					Rating: "3",
				},
			},
		},
	}
	m := make(map[string]Student, len(st))

	for _, v := range st {
		m[v.Name] = v
	}
	printSt(m)
}

func printSt(s map[string]Student) {
	for _, c := range s {
		fmt.Printf("Имя: %s\n", c.Name)
		fmt.Printf("Возраст: %d\n\n", c.Age)
		for _, s := range c.Module {
			fmt.Printf("Предмет: %s\n", s.Class)
			fmt.Printf("Оценка: %s\n\n", s.Rating)
		}
	}
}
