package main

import "fmt"

type Animal struct {
	Name, Vid string
	Character []Characterisic
}

type Characterisic struct {
	Name  string
	Value int
}

func main() {
	anml := []Animal{
		{
			Name: "gepard",
			Vid:  "cat",
			Character: []Characterisic{
				{
					Name:  "speed",
					Value: 110,
				},
			},
		},
		{
			Name: "elephant",
			Vid:  "носатый",
			Character: []Characterisic{
				{
					Name:  "strange",
					Value: 200,
				},
			},
		},
		{
			Name: "mouse",
			Vid:  "мелкий",
			Character: []Characterisic{
				{
					Name:  "funny",
					Value: 50,
				},
			},
		},
	}
	m := make(map[string]Animal)

	//проходим по срезу и добавляем его в мапу 'm'
	for _, v := range anml {
		m[v.Name] = v
	}
	printAn(m)
}

// создаем функцию для вывода
func printAn(m map[string]Animal) {
	for _, i := range m {
		fmt.Printf("Животное: %s\n", i.Name)
		fmt.Printf("Вид: %s\n\n", i.Vid)
		for _, s := range i.Character {
			fmt.Printf("Характеристика: %s\n", s.Name)
			fmt.Printf("Значение: %d\n--------------\n", s.Value)
		}
	}
}
