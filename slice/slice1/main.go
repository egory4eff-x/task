package main

import (
	"fmt"
	"sort"
)

func main() {
	word := "smile"
	sl := []int{11, 3, 0, 51, 8, 234, 0, 5, 3, 222, 5, 17, 0}
	sK := []int{3, 5, 6, 7, 3, 47, 4, 4, 7, 2}
	sT := []string{"da", "da", "you", "two", "one", "rock", "you"}
	fmt.Println(sl)
	maxInt(sl)
	fmt.Println(duble(sT))
	sorter(sl)
	fmt.Println(oneDuble(sl, sK))
	fmt.Println(recover2(sl))
	fmt.Println(recover(sl))
	fmt.Println(zero(sl))
	fmt.Println(strRecover(word))
	fmt.Println(bools(sK, 3))
	fmt.Println(firstElement(sK, 4))
	fmt.Println(deleter(sK))
}

// нахождение максимального значения в []int
func maxInt(s []int) {
	max := 0
	for _, v := range s {
		if max < v {
			max = v
		}
	}
	fmt.Println(max)
}

// удаление дубликатов строк из []string
func duble(s []string) []string {
	sort.Strings(s)
	res := make([]string, 0)
	for i := 0; i < len(s); i++ {
		if (i > 0 && s[i-1] == s[i]) || len(s[i]) == 0 {
			continue
		}
		res = append(res, s[i])
	}
	return res
}

// сортировка []int
func sorter(s []int) {
	sort.Ints(s)
	fmt.Println(s)
}

// удаление дубликатов 2 - []int
func oneDuble(s, a []int) []int {
	d := append(s, a...)
	sort.Ints(d)
	fmt.Println(d)
	res := make([]int, 0)
	for i := 0; i < len(d); i++ {
		if i > 0 && d[i] == d[i-1] {
			continue
		}
		res = append(res, d[i])
	}
	return res
}

// возврат слайса с четными значениями []int
func recover2(s []int) []int {
	res := make([]int, 0)
	for i := 0; i < len(s); i++ {
		if s[i]%2 == 0 {
			res = append(res, s[i])
		}
	}
	return res
}

// возврат слайса в обратном порядке []int
func recover(s []int) []int {
	fmt.Println(s)
	for i := 0; i < len(s)/2; i++ {
		s[i], s[len(s)-1-i] = s[len(s)-1-i], s[i]
	}
	return s
}

// строка в обратном порядке
func strRecover(s string) string {
	fmt.Println(s)

	// решение через пустую стоку, с добавлением нового элемента
	//res := ""
	//for _, i := range s {
	//	res = s(i) + res
	//}
	//return res

	// решение через байтовый срез
	res := []byte(s)
	for i := 0; i < len(res)/2; i++ {
		res[i], res[len(res)-1-i] = res[len(res)-1-i], res[i]
	}
	return string(res)
}

// удаление "0" из слайса
func zero(s []int) []int {
	fmt.Println(s)
	res := make([]int, 0)
	for i := 0; i < len(s); i++ {
		if s[i] == 0 {
			continue
		}
		res = append(res, s[i])
	}
	return res
}

// проверить на условие слайс
func bools(s []int, n int) bool {
	for i, _ := range s {
		if s[i] == n {
			return true
		}
	}
	return false
}

// возврат нового слайса, состоящего из N элементов
func firstElement(s []int, n int) []int {
	sl := make([]int, 0)
	for i := 0; i < n; i++ {
		sl = append(sl, s[i])
	}
	return sl
}

// удаление элементов, не соответствующие условию
func deleter(s []int) []int {
	sl := make([]int, 0)

	for _, v := range s {
		if v < 10 {
			continue
		}
		sl = append(sl, v)
	}
	return sl
}
